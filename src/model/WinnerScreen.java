package model;

import constants.PawnConstants;
import controllers.GameFieldController;
import controllers.WinnerScreenController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Окно, отображающееся в конце игры
 */
public class WinnerScreen {

    public void newWinnerScreen(int winner, GameFieldController gameFieldController) {

        Stage window = new Stage(StageStyle.UNDECORATED);
        window.initModality(Modality.APPLICATION_MODAL);
        window.setResizable(false);

        WinnerScreenController.setGameFieldController(gameFieldController);


        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/WinnerScreen.fxml"));
            fxmlLoader.setResources(ResourceBundle.getBundle("bundles.locale", Locale.getDefault()));

            if (winner == PawnConstants.COMPUTER)
                window.setTitle(fxmlLoader.getResources().getString("WinCompTitleText"));
            else window.setTitle(fxmlLoader.getResources().getString("WinUsrTitleText"));


            if (winner == PawnConstants.COMPUTER)
                WinnerScreenController.setValues(fxmlLoader.getResources().getString("Computer"), new Image("/images/comp.png"),
                        "-fx-background-color: linear-gradient(#ff5400, #be1d00);" +
                                "    -fx-background-insets: 0;" +
                                "    -fx-text-fill: white;");
            else WinnerScreenController.setValues(fxmlLoader.getResources().getString("User"), new Image("/images/user.png"),
                    "-fx-background-color:" +
                            "        linear-gradient(#f0ff35, #a9ff00)," +
                            "        radial-gradient(center 50% -40%, radius 200%, #b8ee36 45%, #80c800 50%);" +
                            "    -fx-background-insets: 0, 1;" +
                            "    -fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.4) , 5, 0.0 , 0 , 1 );" +
                            "    -fx-text-fill: #395306;");

            Parent root = fxmlLoader.load();
            window.setScene(new Scene(root));
            WinnerScreenController.setCurrentStage(window);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

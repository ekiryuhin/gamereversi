package model.processors;

import constants.PawnConstants;
import constants.PriorityConstants;
import model.game.elements.Cell;
import model.game.elements.GameField;
import model.game.elements.Pawn;
import model.processors.TurnProcessor;
import model.processors.UserProcessor;

import java.util.ArrayList;

/**
 * Класс, реализующий ИИ
 */
class AIProcessor {

    private final GameField field;

    public AIProcessor(GameField field) {
        this.field = field;
    }

    /**
     * Выбирает из всех возможных вариантов ходов оптимальный
     * @return Возвращает ячейку, в которую делется ход
     */
    public Cell getTurn() {

        TurnProcessor.updateCellsPriorities(field);
        Cell bestTurn = null;
        int priority = -1;
        ArrayList<Cell> turns = TurnProcessor.getTurns(field, PawnConstants.COMPUTER);      // все возможные ходы
        ArrayList<Cell> badTurns = new ArrayList<>();                                       // плохие ходы
        ArrayList<Cell> goodTurns = new ArrayList<>();                                      // хорошие ходы
        ArrayList<Cell> unrequitedMoves = new ArrayList<>();                                // безответные ходы

        for (Cell currCell : turns)
            if (currCell.getPriority().ordinal() >= priority) {

                if (!(new UserProcessor(modellingTurn(currCell)).hasAvailableTurns())) {
                    unrequitedMoves.add(currCell);
                } else if (currCell.getPriority().ordinal() < PriorityConstants.NORMAL_PRIORITY.ordinal()) {
                    badTurns.add(currCell);
                } else goodTurns.add(currCell);

                priority = currCell.getPriority().ordinal();
            }

        if (unrequitedMoves.size() == 0 && goodTurns.size() == 0)
            bestTurn = getBadForPlayerTurn(badTurns);
        else if (unrequitedMoves.size() == 0)
            bestTurn = getBadForPlayerTurn(goodTurns);
        else if (goodTurns.size() == 0) {
            priority = -1;
            for (Cell currCell : unrequitedMoves)
                if (currCell.getPriority().ordinal() >= priority) {
                    bestTurn = currCell;
                    priority = currCell.getPriority().ordinal();
                }
        } else {
            Cell bestUnrequitedMove = unrequitedMoves.get(0);
            Cell bestGoodMove;

            priority = -1;
            for (Cell currCell : unrequitedMoves)
                if (currCell.getPriority().ordinal() >= priority) {
                    bestUnrequitedMove = currCell;
                    priority = currCell.getPriority().ordinal();
                }

            bestGoodMove = getBadForPlayerTurn(goodTurns);

            if (Math.abs(bestGoodMove.getPriority().ordinal() - bestUnrequitedMove.getPriority().ordinal()) <= 1)
                bestTurn = bestUnrequitedMove;
            else bestTurn = bestGoodMove;
        }

        return bestTurn;
    }




    /**
     * Выбирает из предложенных наименее удобный для пользователя ход
     * @param turns - список возможных ходов
     * @return Возвращает клетку с наименьшим приоритетом хода у пользователя
     */
    private Cell getBadForPlayerTurn(ArrayList<Cell> turns) {

        Cell badForPlayerTurn = turns.get(0);
        PriorityConstants minUserPriority = PriorityConstants.MAXIMAL_PRIORITY;

        for (Cell currCell : turns) {
            GameField model = modellingTurn(currCell);
            Cell maxPriorCell = getMaxPriorityUsersAvailableCell(model);
            if (maxPriorCell.getPriority().ordinal() <= minUserPriority.ordinal()) {
                minUserPriority =  maxPriorCell.getPriority();
                badForPlayerTurn = currCell;
            }
        }

        return badForPlayerTurn;
    }




    /**
     * Моделирует игровове поле после определенного хода
     * @param cell - клетка, в которую будет установлена фишка
     * @return Возвращает игровое поле, в котором произошли изменения, соответствующие ходу в переданную клетку
     */
    private GameField modellingTurn(Cell cell) {
        GameField model = new GameField();
        Cell[][] modelCells = new Cell[GameField.getSizeX()][GameField.getSizeY()], oldCells = field.getGameField();

        for (int i = 0; i < GameField.getSizeX(); i++)
            for (int j = 0; j < GameField.getSizeY(); j++)
                        modelCells[i][j] = new Cell(new Pawn(oldCells[i][j].getPawn()), oldCells[i][j].getCoordX(), oldCells[i][j].getCoordY(), oldCells[i][j].getPriority());

        model.setGameField(modelCells);
        ArrayList<Cell> turnedPawns = TurnProcessor.getTurnedOverPawns(model, cell, PawnConstants.COMPUTER);

        model.getGameField()[cell.getCoordX()][cell.getCoordY()].setPawn(new Pawn(PawnConstants.COMPUTER));
        for (Cell currCell : turnedPawns)
            currCell.rollOver();

        return model;
    }




    /**
     * Получение клетеки игрового поля, в которую пользователь может сделать ход, с максимально возможным приоритетом
     * @param field - игровое поле
     * @return Возвращает клетку, с максимальным приоритетом хода для пользователя
     */
    private Cell getMaxPriorityUsersAvailableCell(GameField field) {

        PriorityConstants maxPriority = PriorityConstants.MINIMAL_PRIORITY;

        ArrayList<Cell> availableTurns = TurnProcessor.getTurns(field, PawnConstants.USER);
        Cell bestTurn = availableTurns.get(0);

        for (Cell currCell : availableTurns)
            if (currCell.getPriority().ordinal() >= maxPriority.ordinal()) {
                    maxPriority = currCell.getPriority();
                    bestTurn = currCell;
            }

        return bestTurn;
    }




    /**
     * Проверяет наличие ходов
     * @return Возвращает true, если есть возможность сделать ход
     */
    public boolean hasAvailableTurns() {
        return (TurnProcessor.getTurns(field, PawnConstants.COMPUTER).size() > 0);
    }




    /**
     * Возвращает ячейки, которые должны быть перевернуты в результате хода
     * @param turn - клетка, в которую сделан ход
     * @return Возвращает список ячеек, которые дорлжны быть перевернуты
     */
    public ArrayList<Cell> getRollOver(Cell turn) {
        return (TurnProcessor.getTurnedOverPawns(field, turn, PawnConstants.COMPUTER));
    }

}

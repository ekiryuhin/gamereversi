package model.processors;

import constants.PawnConstants;
import model.game.elements.Cell;
import model.game.elements.GameField;
import model.game.elements.Pawn;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Содержит функции для управления основными функциями приложения
 */
public class GameProcessor {

    private final GameField field;
    private final AIProcessor aiProcessor;
    private final UserProcessor userProcessor;
    private final Stack<GameField> stactTurns;

    public GameProcessor() {
        field = new GameField();
        aiProcessor = new AIProcessor(field);
        userProcessor = new UserProcessor(field);
        stactTurns = new Stack<>();

    }


    public Cell[][] getField() {
        return field.getGameField();
    }


    /**
     * Ход компьютера
     */
    public void AITurn() {
        Cell aiTurn = aiProcessor.getTurn();
        aiTurn.setPawn(new Pawn(PawnConstants.COMPUTER));

        for (Cell currCell : aiProcessor.getRollOver(aiTurn))
            currCell.rollOver();

    }

    /**
     * Добавляет в стэк ходов новый ход
     *
     * @param field - поле после хода
     */
    private void addTurn(GameField field) {
        GameField inStackField = new GameField();
        Cell[][] oldField = field.getGameField();


        for (int i = 0; i < GameField.getSizeX(); i++)
            for (int j = 0; j < GameField.getSizeY(); j++)
                inStackField.getGameField()[i][j] = new Cell(new Pawn(oldField[i][j].getPawn()), oldField[i][j].getCoordX(), oldField[i][j].getCoordY(), oldField[i][j].getPriority());

        stactTurns.push(inStackField);
    }


    public void undoTurn() {
        if (stactTurns.size() > 0) {
            field.setGameField(stactTurns.peek().getGameField());
            stactTurns.pop();

        }
    }

    /**
     * Возврщает возможные ходы человека
     *
     * @return Возвращает список ячеек
     */
    public ArrayList<Cell> getUserAvailablelCells() {
        return userProcessor.getPossibleTurns();
    }


    /**
     * Ход игрока
     * Реагирует на выбор ячейки игрового поля
     *
     * @param index1 - координата ячейки, в которую делается ход, по X
     * @param index2 - координата ячейки, в которую делается ход, по Y
     */
    public void userTurn(int index1, int index2) {

        addTurn(field);
        field.getGameField()[index1][index2].setPawn(new Pawn(PawnConstants.USER));
        for (Cell currCell : userProcessor.getRollOver(field.getGameField()[index1][index2]))
            currCell.rollOver();


    }


    /**
     * Проверяет не закончена ли игра
     *
     * @return Возвращает true, если игра закончена
     */
    public boolean isGameEnd() {
        return (!aiProcessor.hasAvailableTurns() && !userProcessor.hasAvailableTurns());
    }


    /**
     * Проверка невозможен ли ход компьютера
     *
     * @return Возвращает true, если ход компьютера невозможен
     */
    public boolean isAITurnImpossible() {
        return !aiProcessor.hasAvailableTurns();
    }


    /**
     * Проверка невозможен ли ход игрока
     *
     * @return Возвращает true, если ход игрока невозможен
     */
    public boolean isUserTurnImpossible() {
        return !userProcessor.hasAvailableTurns();
    }



    /**
     * Считает количество фишек у заданной стороны
     *
     * @param parent - сторона, для которой ведется рассчет (компьютер/пользователь)
     * @return Возвращает количество фишек
     */
    private int countPawns(int parent) {

        int count = 0;

        Cell[][] cells = field.getGameField();
        for (int i = 0; i < GameField.getSizeX(); i++)
            for (int j = 0; j < GameField.getSizeY(); j++)
                if (cells[i][j].getPawn() == parent)
                    count++;

        return count;
    }

    /**
     * Проверяет у какой стороны на данный момент больше фишек
     *
     * @return Возвращает сторону, имеющее большее число фишек, если количество равно, возвращает EMPTY
     */
    public int whoHasMorePawns() {
        if (countPawns(PawnConstants.USER) == countPawns(PawnConstants.COMPUTER))
            return PawnConstants.EMPTY;
        else if (countPawns(PawnConstants.USER) > countPawns(PawnConstants.COMPUTER))
            return PawnConstants.USER;
        else
            return PawnConstants.COMPUTER;
    }



    public int getFieldSizeX() {
        return GameField.getSizeX();
    }

    public int getFieldSizeY() { return GameField.getSizeY(); }

}

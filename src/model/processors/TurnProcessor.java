package model.processors;

import constants.CellConstants;
import constants.PawnConstants;
import constants.PriorityConstants;
import model.game.elements.Cell;
import model.game.elements.GameField;

import java.util.*;

/**
 * Описывает операции, необходимые для поиска возможных ходов
 */
class TurnProcessor {

    /**
     * Возвращает все ячейки, в которые можно сделать ход
     * @param field - игровое поле
     * @param parent - сторона, для которой ведется рассчет (компьютер/пользователь)
     * @return Возвращает список доступных для хода ячеек
     */
    public static ArrayList<Cell> getTurns(GameField field, int parent) {

        ArrayList<Cell> availableCells, cellsForCheck;

        cellsForCheck = getCellsForCheck(field, parent);
        availableCells = new ArrayList<>();

        /*
         * В цикле проверяется корректность хода в данную ячейку по каждому вектору.
         * Если хотя бы в одном случае ход возможен, ячейка считается доступной для хода.
         */
        for (Cell currCell : cellsForCheck) {
            boolean isAvailable = false;
            int vector = CellConstants.NORTH;
            while ((vector <= CellConstants.NORTHWEST) && !isAvailable) {
                if (checkVector(field, currCell, parent, vector)) {
                    isAvailable = true;
                    availableCells.add(currCell);
                }
                vector++;
            }
        }

        return availableCells;
    }




    /**
     * Проверяет по данному вектору является ли ход в заданную ячейку корректным в соответствии с правилами игры
     * @return возвращает true если ход корректен
     */
    private static boolean checkVector(GameField field, Cell cell, int parent, int vector) {

        boolean isAvailable = false;
        int coeffX = 0, coeffY = 0;

        /*
         * Выставляет коэффициенты перехода на соседнюю ячейку в соответствии с заданным вектором
         */
        switch (vector) {
            case CellConstants.NORTH:
                coeffY = -1;
                break;
            case CellConstants.NORTHEAST:
                coeffY = -1;
                coeffX = 1;
                break;
            case CellConstants.EAST:
                coeffX = 1;
                break;
            case CellConstants.SOUTHEAST:
                coeffY = 1;
                coeffX = 1;
                break;
            case CellConstants.SOUTH:
                coeffY = 1;
                break;
            case CellConstants.SOUTHWEST:
                coeffY = 1;
                coeffX = -1;
                break;
            case CellConstants.WEST:
                coeffX = -1;
                break;
            case CellConstants.NORTHWEST:
                coeffY = -1;
                coeffX = -1;
                break;
        }

        Cell[][] cells = field.getGameField();
        int coordX = cell.getCoordX() + coeffX;
        int coordY = cell.getCoordY() + coeffY;
        int enemy;

        /*
         * Значение enemy устанвливается обратным значнию parent
         */
        if (parent == PawnConstants.USER)
            enemy = PawnConstants.COMPUTER;
        else enemy = PawnConstants.USER;

        /*
         * Если в ближайшей ячейке установлена фишка соперника, запускается цикл, ищущий ячейку игрока.
         * Если ячейка игрока найдена без прерываний на пустую ячейку, то ячейка является возможной для хода.
         */
        if ((isCorrectCoord(coordX, coordY)))
            if (cells[coordX][coordY].getPawn() == enemy) {
                coordX += coeffX;
                coordY += coeffY;
                while ((isCorrectCoord(coordX, coordY)) && !isAvailable && (cells[coordX][coordY].getPawn() != PawnConstants.EMPTY)) {
                    if (cells[coordX][coordY].getPawn() == parent)
                        isAvailable = true;

                    coordX += coeffX;
                    coordY += coeffY;
                }
            }

        return isAvailable;
    }




    /**
     * Возвращает список ячеек, занятых одной из сторон
     * @param field - игровое поле
     * @param parent - сторона, для которой ведется рассчет (компьютер/пользователь)
     * @return Возвращает список ячеек, занятых одной из сторон
     */
    private static ArrayList<Cell> getHavingCells(GameField field, int parent) {
        ArrayList<Cell> havingCells = new ArrayList<>();
        Cell[][] cells = field.getGameField();

        for (int i = 0; i < GameField.getSizeX(); i++)
            for (int j = 0; j < GameField.getSizeY(); j++)
                if (cells[i][j].getPawn() == parent)
                    havingCells.add(cells[i][j]);

        return  havingCells;
    }



    /**
     * Возвращает список уникальных ячеек для последующей проверки на возможность хода в них
     * @param field - игровое поле
     * @param parent - сторона, для которой ведется рассчет (компьютер/пользователь)
     * @return Возвращает список ячеек
     */
    private static ArrayList<Cell> getCellsForCheck(GameField field, int parent) {
        ArrayList<Cell> cellsForCheck;
        ArrayList<Cell> enemyCells;
        int enemy;

        /*
         * Переменная enemy хранит идентификатор оппонента
         */
        if (parent == PawnConstants.USER)
            enemy = PawnConstants.COMPUTER;
        else enemy = PawnConstants.USER;

        cellsForCheck = new ArrayList<>();
        enemyCells = getHavingCells(field, enemy);

        /*
         * Проверяются все ячейки ряжом с фишками оппонента
         * Пустые добавляются в список cellsForCheck
         */
        for (Cell currCell : enemyCells) {
            cellsForCheck.addAll(getAdjacentAvailableCells(field, currCell));
        }

        /*
         * Из списка удаляются повторы
         */
        Set<Cell> tempSet = new HashSet<>(cellsForCheck);
        cellsForCheck = new ArrayList<>(tempSet);

        return cellsForCheck;
    }




    /**
     * Возвращает список соседних пустых ячеек
     * @param field - игровое поле
     * @param cell - ячейка
     * @return Возвращает список ячеек
     */
    private static ArrayList<Cell> getAdjacentAvailableCells(GameField field, Cell cell) {

        ArrayList<Cell> adjacentAvailableCells = new ArrayList<>();
        Cell[][] cells = field.getGameField();

        int coordX = cell.getCoordX();
        int coordY = cell.getCoordY();

        /*
         * В список добавляются все соседние ячейки
         */
        if ((coordX - 1 >= 0))
             adjacentAvailableCells.add(cells[coordX - 1][coordY]);
        if (coordY - 1 >= 0)
            adjacentAvailableCells.add(cells[coordX][coordY - 1]);
        if ((coordX - 1 >= 0) && (coordY - 1 >= 0))
            adjacentAvailableCells.add(cells[coordX - 1][coordY - 1]);
        if (coordX + 1 < GameField.getSizeX())
            adjacentAvailableCells.add(cells[coordX + 1][coordY]);
        if (coordY + 1 < GameField.getSizeY())
            adjacentAvailableCells.add(cells[coordX][coordY + 1]);
        if ((coordX + 1 < GameField.getSizeX()) && (coordY + 1 < GameField.getSizeY()))
            adjacentAvailableCells.add(cells[coordX + 1][coordY + 1]);
        if ((coordX + 1 < GameField.getSizeX()) && (coordY - 1 >= 0))
            adjacentAvailableCells.add(cells[coordX + 1][coordY - 1]);
        if ((coordX - 1 >= 0) && (coordY + 1 < GameField.getSizeY()))
            adjacentAvailableCells.add(cells[coordX - 1][coordY + 1]);


        /*
         * Из списка удаляются все непустые ячейки
         */
        adjacentAvailableCells.removeIf(temp -> temp.getPawn() != PawnConstants.EMPTY);

        return adjacentAvailableCells;
    }



    /**
     * Проверяет корректность координат
     * @param coordX Координата по Х
     * @param coordY Координата по Y
     * @return Возвращает true, если координаты корректны
     */
    private static boolean isCorrectCoord(int coordX, int coordY) {
        if ((coordX < GameField.getSizeX()) && (coordY < GameField.getSizeY()))
            if ((coordX >= 0) && (coordY >= 0))
                return true;
        return false;
    }




    /**
     * Возвращает ячейки, в которых, в результате хода, будут перевернуты фишки
     * @param field - игровое поле
     * @param turn - ячейка, в которую делается ход
     * @param parent - сторона, для которой ведется рассчет (компьютер/пользователь)
     * @return Возвращает список ячеек
     */
    public static ArrayList<Cell> getTurnedOverPawns(GameField field, Cell turn, int parent) {
        ArrayList<Cell> turnedOverPawns = new ArrayList<>();
        ArrayList<Integer> vectors = new ArrayList<>();

        int vector = CellConstants.NORTH;
        while (vector <= CellConstants.NORTHWEST) {
            if (checkVector(field, turn, parent, vector))
                    vectors.add(vector);
            vector++;
        }

        for (int currVector : vectors) {
            turnedOverPawns.addAll(getTurnedOverByVector(field, turn, parent, currVector)) ;
        }
        return turnedOverPawns;
    }




    /**
     * Возвращает ячейки, в которых, в результате хода, будут перевернуты фишки по заданному вектору
     * @param field - игровое поле
     * @param turn - ячейка, в которую делается ход
     * @param parent - сторона, для которой ведется рассчет (компьютер/пользователь)
     * @param vector - вектор, по которому ведется рассчет
     * @return Возвращает список ячеек
     */
    private static ArrayList<Cell> getTurnedOverByVector(GameField field, Cell turn, int parent, int vector) {

        ArrayList<Cell> turnOver = new ArrayList<>();
        int coeffX = 0, coeffY = 0;

        /*
         * Выставляет коэффициенты перехода на соседнюю ячейку в соответствии с заданным вектором
         */
        switch (vector) {
            case CellConstants.NORTH:
                coeffY = -1;
                break;
            case CellConstants.NORTHEAST:
                coeffY = -1;
                coeffX = 1;
                break;
            case CellConstants.EAST:
                coeffX = 1;
                break;
            case CellConstants.SOUTHEAST:
                coeffY = 1;
                coeffX = 1;
                break;
            case CellConstants.SOUTH:
                coeffY = 1;
                break;
            case CellConstants.SOUTHWEST:
                coeffY = 1;
                coeffX = -1;
                break;
            case CellConstants.WEST:
                coeffX = -1;
                break;
            case CellConstants.NORTHWEST:
                coeffY = -1;
                coeffX = -1;
                break;
        }

        Cell[][] cells = field.getGameField();
        int coordX = turn.getCoordX() + coeffX;
        int coordY = turn.getCoordY() + coeffY;

        /*
         * В цикле в список заносятся все ячейки противника, которые будут перевернуты
         */

        while ((cells[coordX][coordY].getPawn() != parent)) {
            turnOver.add(cells[coordX][coordY]);
            coordX += coeffX;
            coordY += coeffY;
        }


        return turnOver;
    }


    /**
     * Переопределяет приоритеты ячеек, в заисимости от ситуации на игровом поле
     * @param field - Игровое поле
     */
    public static void updateCellsPriorities(GameField field) {


        updateBorderCellsPriorities(getColumn(field, 0));
        updateBorderCellsPriorities(getColumn(field, 7));
        updateBorderCellsPriorities(field.getGameField()[0]);
        updateBorderCellsPriorities(field.getGameField()[7]);

    }

    /**
     * Возвращает столбец ячеек игрового поля по его номеру
     * @param field - игровое поле
     * @param columnNumber - номер столбца
     * @return Возвращает массив ячеек
     */
    private static Cell[] getColumn(GameField field, int columnNumber) {
        Cell[] column = new Cell[8];

        for (int i = 0; i < GameField.getSizeX(); i++)
            for (int j = 0; j < GameField.getSizeY(); j++)
                if (j == columnNumber)
                    column[i] = field.getGameField()[i][j];

        return column;
    }


    /**
     * Обновляет приоритеты ячеек в боковом столбце/строчке
     * @param border - Массив ячеек
     */
    private static void updateBorderCellsPriorities(Cell[] border) {

        if (border[3].getPawn() == PawnConstants.EMPTY)
            if (border[2].getPawn() == border[4].getPawn() && border[4].getPawn() == PawnConstants.USER)
                border[3].setPriority(PriorityConstants.BIG_PRIORITY);

        if (border[4].getPawn() == PawnConstants.EMPTY)
            if (border[3].getPawn() == border[5].getPawn() && border[5].getPawn() == PawnConstants.USER)
                border[4].setPriority(PriorityConstants.BIG_PRIORITY);
    }


}
package model.processors;

import constants.PawnConstants;
import model.game.elements.Cell;
import model.game.elements.GameField;
import model.processors.TurnProcessor;

import java.util.ArrayList;

/**
 *
 */
class UserProcessor {

    private final GameField field;

    public UserProcessor(GameField field) {
        this.field = field;
    }

    /**
     * Возвращает все возможные ходы, которые может сделать игрок
     * @return Возвращает список ячеек, в которые игрок может сделать ход
     */
    public ArrayList<Cell> getPossibleTurns() {
        return TurnProcessor.getTurns(field, PawnConstants.USER);
    }

    public ArrayList<Cell> getRollOver(Cell turn) {
        return TurnProcessor.getTurnedOverPawns(field, turn, PawnConstants.USER);
    }

    /**
     * Проверяет наличие ходов
     * @return Возвращает true, если есть возможность сделать ход
     */
    public boolean hasAvailableTurns() {
        return (TurnProcessor.getTurns(field, PawnConstants.USER).size() > 0);
    }

}

package model;

import controllers.WinnerScreenController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Основной экран приложения
 */

public class  MainScreen extends Application {

    private static final int minHeight = 700;
    private static final int minWidth = 500;


    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/fxml/mainscreen.fxml"));
        fxmlLoader.setResources(ResourceBundle.getBundle("bundles.locale", Locale.getDefault()));
        Parent root = fxmlLoader.load();
        primaryStage.setTitle(fxmlLoader.getResources().getString("GameName"));
        primaryStage.setMinHeight(minHeight);
        primaryStage.setMinWidth(minWidth);
        primaryStage.setScene(new Scene(root, minWidth, minHeight));
        WinnerScreenController.setGeneralStage(primaryStage);
        primaryStage.show();
    }



    public static void main(String[] args) {
        launch(args);
    }
}

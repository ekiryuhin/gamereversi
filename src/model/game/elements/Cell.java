package model.game.elements;

import constants.PawnConstants;
import constants.PriorityConstants;

/**
 * Класс, описывающий ячейку игрового поля
 */
public class Cell {

    private Pawn pawn;
    private int coordX;
    private int coordY;

    public Cell() {}


    public Cell (Pawn pawn, int coordX, int coordY, PriorityConstants priority) {

        this.priority = priority;
        this.pawn = pawn;
        this.coordX = coordX;
        this.coordY = coordY;
    }



    /**
     * Возвращает тип хранимой фишки
     * Если фишки нет, возвращает EMPTY
     * @return Возвращает кому принадлежит фишка, компьютеру или игроку
     */
    public int getPawn() {

        if (pawn != null) {
            return pawn.getParent();
        }

        return PawnConstants.EMPTY;
    }

    public void rollOver() {
        if (pawn.getParent() == PawnConstants.COMPUTER)
            pawn.setParent(PawnConstants.USER);
        else pawn.setParent(PawnConstants.COMPUTER);
    }

    public void setPawn(Pawn pawn) {
        this.pawn = pawn;

    }

    public int getCoordX() {
        return coordX;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }


    public PriorityConstants getPriority() {
        return priority;
    }

    public void setPriority(PriorityConstants priority) {
        this.priority = priority;
    }

    /**
     * Приоритет каждой ячейки
     */
    private PriorityConstants priority;



}

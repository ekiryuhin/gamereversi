package model.game.elements;


/**
 * Описывает фишку
 */
public class Pawn {
    private int parent;

    public Pawn(int parent) {
        this.parent = parent;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }
}


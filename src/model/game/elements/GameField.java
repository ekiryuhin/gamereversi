package model.game.elements;

import constants.PawnConstants;
import constants.PriorityConstants;

/**
 * Класс, описывающий игровое поля
 */

public class GameField {

    /**
     * Константы, задающие размеры игрового поля
     */

    private static final int sizeX = 8;
    private static final int sizeY = 8;

    public static int getSizeX() {
        return sizeX;
    }

    public static int getSizeY() {
        return sizeY;
    }



    /**
     *  Массив, хранящий ячейки игрового поля
     */
     private final Cell field[][] = new Cell[sizeX][sizeY];





    /**
     * Создание нового GameField
     */
    public GameField() {
        for (int i = 0; i < sizeX; i++)
            for (int j  = 0; j < sizeY; j++) {
                field[i][j] = new Cell();
                field[i][j].setCoordX(i);
                field[i][j].setCoordY(j);
            }

        /*
          Установка фишек, стоящих в начале игры
         */
        field[3][3].setPawn(new Pawn(PawnConstants.COMPUTER));
        field[3][4].setPawn(new Pawn(PawnConstants.USER));
        field[4][3].setPawn(new Pawn(PawnConstants.USER));
        field[4][4].setPawn(new Pawn(PawnConstants.COMPUTER));

        /*
         * Установка максимальных приоритетов для угловых ячеек
         */
        field[0][0].setPriority(PriorityConstants.MAXIMAL_PRIORITY);
        field[0][7].setPriority(PriorityConstants.MAXIMAL_PRIORITY);
        field[7][0].setPriority(PriorityConstants.MAXIMAL_PRIORITY);
        field[7][7].setPriority(PriorityConstants.MAXIMAL_PRIORITY);

        /*
          Установка высоких приоритетов соответствующим ячейкам
         */
        field[0][2].setPriority(PriorityConstants.BIG_PRIORITY);
        field[0][5].setPriority(PriorityConstants.BIG_PRIORITY);
        field[2][0].setPriority(PriorityConstants.BIG_PRIORITY);
        field[5][0].setPriority(PriorityConstants.BIG_PRIORITY);
        field[2][7].setPriority(PriorityConstants.BIG_PRIORITY);
        field[5][7].setPriority(PriorityConstants.BIG_PRIORITY);
        field[7][2].setPriority(PriorityConstants.BIG_PRIORITY);
        field[7][5].setPriority(PriorityConstants.BIG_PRIORITY);


        /*
          Установка минимальных приоритетов соответствующим ячейкам
         */
        field[0][1].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[1][0].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[0][6].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[6][0].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[0][1].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[7][1].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[1][7].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[6][7].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[7][6].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[1][1].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[1][6].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[6][1].setPriority(PriorityConstants.MINIMAL_PRIORITY);
        field[6][6].setPriority(PriorityConstants.MINIMAL_PRIORITY);

        /*
          Установка низких приоритетов соответствующим ячейкам
         */
        field[0][3].setPriority(PriorityConstants.LOW_PRIORITY);
        field[0][4].setPriority(PriorityConstants.LOW_PRIORITY);
        field[3][0].setPriority(PriorityConstants.LOW_PRIORITY);
        field[4][0].setPriority(PriorityConstants.LOW_PRIORITY);
        field[3][7].setPriority(PriorityConstants.LOW_PRIORITY);
        field[4][7].setPriority(PriorityConstants.LOW_PRIORITY);
        field[7][3].setPriority(PriorityConstants.LOW_PRIORITY);
        field[7][4].setPriority(PriorityConstants.LOW_PRIORITY);

        /*
        Остальным ячейкам присваивается обычный приоритет
         */
        for (int i = 0; i < sizeX; i++)
            for (int j  = 0; j < sizeY; j++) {
               if (field[i][j].getPriority() == null)
                   field[i][j].setPriority(PriorityConstants.NORMAL_PRIORITY);
            }
    }


    /**
     * Возвращает игровое поле
     * @return Возвращает массив ячеек
     */

    public Cell[][] getGameField() {

        return field;
    }


    /**
     * Устанавливает заданное игровое поле
     * @param field - Двумерный масив ячеек игрового поля
     */
    public void setGameField(Cell[][] field) {

        for (int i = 0; i < sizeX; i++)
            System.arraycopy(field[i], 0, this.field[i], 0, sizeY);


    }

}

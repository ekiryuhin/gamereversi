package constants;

/**
 * Константы, необходимые для сравнения приоритетов ячеек игрового поля
 */
public enum PriorityConstants {

    MINIMAL_PRIORITY,
    LOW_PRIORITY,
    NORMAL_PRIORITY,
    BIG_PRIORITY,
    MAXIMAL_PRIORITY

}

package constants;

/**
 * Интерфейс, содержащий константы для описания расположения фишек
 * относительно друг друга
 */
public final class CellConstants {

    public static final int NORTH = 0;
    public static final int NORTHEAST = 1;
    public static final int EAST = 2;
    public static final int SOUTHEAST = 3;
    public static final int SOUTH = 4;
    public static final int SOUTHWEST = 5;
    public static final int WEST = 6;
    public static final int NORTHWEST = 7;

}

package constants;

/**
 * Костанты для работы с цветом фишек
 */
public final class PawnConstants {
    public static final int USER = 1;
    public static final int COMPUTER = 2;
    public static final int EMPTY = 3;
}

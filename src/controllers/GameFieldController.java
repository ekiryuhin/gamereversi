package controllers;


import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import model.WinnerScreen;
import model.game.elements.Cell;
import model.game.elements.GameField;
import model.processors.GameProcessor;
import constants.PawnConstants;
import view.CellView;
import view.PawnView;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Контроллер визуального представления игрового поля
 */


public class GameFieldController{

    @FXML
    private GridPane gamefield;

    private GameProcessor gameProcessor;
    private CellView cells[][] = null;
    private final WinnerScreen winnerScreen = new WinnerScreen();



    /**
     * Заполняет игровое поле панелями под фишки и устанавливает каждой панели событие по нажатию на нее,
     *  иницализирует gameProcessor и подсчечивает доступные для хода игрока ячейки
     */
    public void newGame() {

        if (cells != null)
            clearField();

        cells = new CellView[8][8];
        gameProcessor = new GameProcessor();
        fillField();
        fullUpdate();
    }

    /**
     * Ход компьютера
     */
    private void AITurn() {

        gameProcessor.AITurn();
        fullUpdate();
    }


    /**
     * Удаляет все ячейки игрового поля
     */
    private void clearField() {

        ArrayList<CellView> cellsForRemove = new ArrayList<>();

        for (CellView[] currCell : cells) {
            cellsForRemove.addAll(Arrays.asList(currCell));
        }
        gamefield.getChildren().removeAll(cellsForRemove);
    }

    /**
     * Отмена хода
     */
    public void undo() {

        gameProcessor.undoTurn();
        fullUpdate();
    }

    /**
     * Обновление ячеек игрового поля в соответствии со значениями, получаемыми из gameProcessor
     */
    private void refresh() {

        Cell[][] field = gameProcessor.getField();
        for (int i = 0; i < gameProcessor.getFieldSizeX(); i++)
            for (int j = 0; j < gameProcessor.getFieldSizeY(); j++) {
                if (cells[i][j].getChildren().size() != 0)
                    cells[i][j].getChildren().remove(0);
                if  (field[i][j].getPawn() != PawnConstants.EMPTY)
                  addPawn(cells[i][j], field[i][j].getPawn());
            }
    }




    /**
     * Динамически создает ячейки игрового поля
     */
    private void fillField() {

        CellClickEvent clickEvent = new CellClickEvent(gameProcessor, this);

        for (int i = 0; i < gameProcessor.getFieldSizeX(); i++)
            for (int j = 0; j < gameProcessor.getFieldSizeY(); j++) {
                cells[i][j] = new CellView(i, j, clickEvent);
                cells[i][j].setStyle("-fx-background-color: palegreen");
                gamefield.add(cells[i][j], i, j);

            }
    }


    /**
     * Добавление фишки в ячейку
     * @param pane - панель, в которую добавляется ячейка
     * @param color - цвет
     */
    private void addPawn(CellView pane, int color) {

        PawnView pawnView = new PawnView(color);
        pane.getChildren().add(pawnView);
        AnchorPane.setBottomAnchor(pawnView, 5.0);
        AnchorPane.setTopAnchor(pawnView, 5.0);
        AnchorPane.setLeftAnchor(pawnView, 5.0);
        AnchorPane.setRightAnchor(pawnView, 5.0);
        pane.setAddable(false);
        pane.setOnMouseClicked(null);
        pane.setDisable(false);
    }


    /**
     * Подсвечивает кнопки, доступные для нажатия игроком
     * @param field - игровое поле
     * @param availableCells - список доступных для хода ячеек
     */
    private void lightAvailableCells(CellView[][] field, ArrayList<Cell> availableCells) {

        for  (Cell iterCell : availableCells) {
           field[iterCell.getCoordX()][iterCell.getCoordY()].setStyle("green");
        }
    }

    /**
     * Делает недоступными для нажатия все кнопки игрового поля
     */
    private void setAllDisable() {

        for (int i = 0; i < gameProcessor.getFieldSizeX(); i++)
            for (int j = 0; j < gameProcessor.getFieldSizeY(); j++)
                if (cells[i][j].getChildren().isEmpty())
                    cells[i][j].setDisable(true);
    }

    /**
     * Отключает подсветку всех ячеек
     */
    private void offLight() {

        for (int i = 0; i < GameField.getSizeX(); i++)
            for (int j = 0; j < GameField.getSizeY(); j++)
                cells[i][j].setStyle("-fx-background-color: palegreen");

    }

    /**
     * Делает доступными для нажатия ячейки поля, доступные для хода игрока
     * @param field - массив ячеек игрового поля
     * @param availableCells - список доступных для хода клеток
     */
    private void enableAvailableCells(CellView[][] field, ArrayList<Cell> availableCells) {

        for  (Cell iterCell : availableCells) {
            field[iterCell.getCoordX()][iterCell.getCoordY()].setDisable(false);
            field[iterCell.getCoordX()][iterCell.getCoordY()].setAddable(true);
        }
    }


    public void updateAfterUserTurn() {


        if (gameProcessor.isGameEnd()) {
            offLight();
        } else {
            offLight();
            setAllDisable();
            refresh();
        }
    }

    public void updateAfterAITurn() {

            if (gameProcessor.isGameEnd()) {
                offLight();
            } else if (gameProcessor.isUserTurnImpossible()) {
                AITurn();
            } else {
                enableAvailableCells(cells, gameProcessor.getUserAvailablelCells());
                lightAvailableCells(cells, gameProcessor.getUserAvailablelCells());
                refresh();
            }
    }

    private void fullUpdate() {

        offLight();
        setAllDisable();
        enableAvailableCells(cells, gameProcessor.getUserAvailablelCells());
        lightAvailableCells(cells, gameProcessor.getUserAvailablelCells());
        refresh();
    }


    public void endGame() {

        int winner = gameProcessor.whoHasMorePawns();

        if (winner != PawnConstants.EMPTY)
            winnerScreen.newWinnerScreen(winner, this);

    }

}

package controllers;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import model.processors.GameProcessor;
import view.CellView;


/**
 * Обработчик нажатия на ячейку игрового поля
 */
class CellClickEvent implements EventHandler<MouseEvent> {

    private final GameProcessor gameProcessor;
    private final GameFieldController gameFieldController;



    public CellClickEvent(GameProcessor gameProcessor, GameFieldController gameFieldController) {
        this.gameFieldController = gameFieldController;
        this.gameProcessor = gameProcessor;


    }

    @Override
        public void handle(MouseEvent event) {
        gameProcessor.userTurn(((CellView)event.getSource()).getIndexX() , ((CellView)event.getSource()).getIndexY());
        gameFieldController.updateAfterUserTurn();

//        if (!gameProcessor.isGameEnd() && !gameProcessor.isUserTurnImpossible()) {
//            gameProcessor.AITurn();
//            if (gameProcessor.isGameEnd())
//                gameFieldController.endGame();
//            else gameFieldController.updateAfterAITurn();
//        } else if (gameProcessor.isGameEnd())
//            gameFieldController.endGame();
//        else {
//            while (!gameProcessor.isGameEnd() && gameProcessor.isUserTurnImpossible())
//                gameProcessor.AITurn();
//
//            if (gameProcessor.isGameEnd())
//                gameFieldController.endGame();
//            else gameFieldController.updateAfterAITurn();
//        }

        if (gameProcessor.isGameEnd())
            gameFieldController.endGame();
        else {
            if (!gameProcessor.isAITurnImpossible()) {
                do {
                    gameProcessor.AITurn();
                } while (!gameProcessor.isGameEnd() && gameProcessor.isUserTurnImpossible() && !gameProcessor.isAITurnImpossible());
                if (gameProcessor.isGameEnd())
                    gameFieldController.endGame();
                else gameFieldController.updateAfterAITurn();
            } else gameFieldController.updateAfterAITurn();
        }
    }
}

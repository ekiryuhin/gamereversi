package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Контроллер винскрина
 */
public class WinnerScreenController  {

    @FXML
    private ImageView winnerImage;

    @FXML
    private Label title;

    @FXML
    private Label winnerName;

    @FXML
    private AnchorPane backPanel;

    private static Stage generalStage;
    private static Stage currentStage;
    private static GameFieldController gameFieldController;
    private static String winName, style;
    private static Image winImage;

    public static void setValues(String winName, Image winImage, String style) {
        WinnerScreenController.winImage = winImage;
        WinnerScreenController.winName = winName;
        WinnerScreenController.style = style;
    }

    public static void setGeneralStage(Stage generalStage) {
        WinnerScreenController.generalStage = generalStage;
    }

    public static void setGameFieldController(GameFieldController gameFieldController) {
        WinnerScreenController.gameFieldController = gameFieldController;
    }

    public static void setCurrentStage(Stage currentStage) {
        WinnerScreenController.currentStage = currentStage;
    }

    @FXML
    private void initialize() {

        Font titleFont = Font.loadFont(getClass().getResource("/fonts/BebasNeueBold.otf").toExternalForm(), 50);
        winnerName.setText(winName);
        winnerName.setFont(titleFont);
        title.setFont(titleFont);
        winnerImage.setImage(winImage);
        backPanel.setStyle(style);

    }

    public void closeGame() {
        generalStage.close();
        currentStage.close();
    }

    public void newGame() {
        currentStage.close();
        gameFieldController.newGame();
    }
}

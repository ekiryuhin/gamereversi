package view;

import constants.PawnConstants;
import javafx.scene.control.Button;


/**
 * Визуальное представление фишек
 */

public class PawnView extends Button {

    public PawnView(int parentColor) {
        super("");

        this.setPickOnBounds(false);
        this.setText("");

        this.getStylesheets().removeAll(this.getStylesheets());
        if (parentColor == PawnConstants.USER)
            this.setStyle("-fx-background-radius: 50%;" + "-fx-background-color: #000000");
        else this.setStyle("-fx-background-radius: 50%;" + "-fx-background-color: #F8F8FF");

    }

}

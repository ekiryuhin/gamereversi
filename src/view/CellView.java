package view;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;



/**
 * Панель, используемая как ячейка игрового поля
 * Унаследована от класса Pane
 */
public class CellView extends AnchorPane {

    /**
     * Координаты ячейки в игровом поле
     */
   private final int indexX;
   private final int indexY;
   private final EventHandler<MouseEvent> eventHandler;


   public CellView(int X, int Y, EventHandler<MouseEvent> clickEvent) {

       indexX = X;
       indexY = Y;
       setOnMouseClicked(clickEvent);
       eventHandler = clickEvent;
   }

   public void setAddable(boolean addable) {
       if (addable)
           setOnMouseClicked(eventHandler);
       else setOnMouseClicked(null);
   }


    public int getIndexX() {
        return indexX;
    }

    public int getIndexY() {
        return indexY;
    }



}
